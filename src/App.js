
import './App.css';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import AppCars from './pages/AppCars';
import Nav from './components/Nav';
import AddCar from './pages/AddCar'



function App() {
  return (
    <div className="App">
      <Router>
        <Nav/>

        <div className="container">
        <Switch>
          
            <Route path='/cars' exact>
              <AppCars/>
            </Route>

            <Route path='/add' exact>
              <AddCar/>
            </Route>

            <Route path='/edit/:id'>
              <AddCar/>
            </Route>
          
        </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
