import axios from 'axios'
import { useParams } from 'react-router-dom';

class CarService {
    constructor(){
        const instance = axios.create(
            {
              baseURL:  'http://localhost:8000/api',
            });
      
            this.client = instance
    }


    async getAll() {

        try {
            const {data} = await this.client.get('cars')
            return data.data 
        } catch (error) {
            console.log(error);
        }

        return[]
    }

    async add(car){
        try {
            const {data} = await this.client.post('cars', car)
    
            return data    
        } catch (error) {
            console.log(error);
        }
    }


    async get(id){
        try {
            const {data} = await this.client.get(`cars/${id}`)
            return data
        } catch (error) {
            console.log(error);
        }
    }


    async edit(id,car){
        try {
            console.log(car);
            const data = await this.client.put(`cars/${id}`, car)
          return data
        } catch (error) {
            console.log(error);
        }
    }


    async delete(id){
        try {
            
            const {data}= await this.client.delete(`cars/${id}`)
            
            return data
        } catch (error) {
            console.log(error);
        }
    }


}


export default new CarService()